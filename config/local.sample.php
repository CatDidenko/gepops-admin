<?php
define('IS_CLI', php_sapi_name() == 'cli');

define('DOMAIN', 'dcodeit.net');
define('BASE_URL', '/geopops-admin/public'); //leave it empty on live
//define('HTTP_SCHEMA', 'http'); // uncomment if you need concrete scheme
define('DEFAULT_HTTP_SCHEMA', 'http');

if (!defined('HTTP_SCHEMA') && !IS_CLI) {
 	define('HTTP_SCHEMA', $_SERVER['REQUEST_SCHEME']);
} else {
	define('HTTP_SCHEMA', DEFAULT_HTTP_SCHEMA);
}

define('URL', HTTP_SCHEMA . '://' . DOMAIN . BASE_URL . '/');

define('DEBUG', true);

define('SUPPORT_EMAIL', 'support@null.dcodeit.net');
define('SITE_NAME', 'Geopops Admin Panel');

//define('SESSION_NAME', '');//do not need this constant on production

define('JS_COMBINE', false);
define('GZIP_OUTPUT', true);

define('ADMIN_LOGIN', 'admin');
define('ADMIN_PASSWORD_HASH', '$2y$12$mvBYqcP5dxA2sWrr1f.zR.jJ/5J685C0s9g9bQh4seQNyx11uPs/6');

define('API_URL', 'http://52.5.13.177:8080/geopops/');
define('FORSQUARE_TOKEN', '0U40D0KKK2QIEQZLGUASKC4JCHWWJ4B1N5C0U1IXSFDFRAPT');

return [
	'database' => [
		'host' => '',
		'name' => '',
		'user' => '',
		'password' => '',
	],
	'cache' => [
		'enabled' => false,
		'namespace' => 'codeit-carcass',
	],
];
