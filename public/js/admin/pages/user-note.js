var userNote = {
	initializePlacesSearchBox : function() {
		var input = /** @type {HTMLInputElement} */(
				document.getElementById('js-address-search'));

		autocomplete = new google.maps.places.Autocomplete(input);
		google.maps.event.addListener(autocomplete, 'place_changed', function () {
			var place = autocomplete.getPlace();
			map.lat = place.geometry.location.lat();
			map.lng = place.geometry.location.lng();
			map.setMarker(map.lat, map.lng, true);
		});
	},
	
	fillForsquareLocation: function() {
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: siteurl + 'admin/user-note/get-forsquare-location',
			data: {
				lat: map.lat,
				lng: map.lng,
			},
			success: function(data) {
				var options = '';
				if (data.status == 'success') {
					for (var i in data.data.list) {
						var selected = (i == 0)? 'selected="selected"' : '';
						var venue = data.data.list[i];
						options += '<option value="'+venue.id+'" '+selected +
							' data-category="' + venue.category + '" '+
							'data-name="' + venue.name + '">' + 
							venue.name + '/' +venue.category + '</option>';
					}
				}
				$('#foursquareVenueId').html(options);
				$('#foursquareVenueId').trigger("liszt:updated");
				userNote.disableForsquareLocationsList();
				userNote.chooseForsquareLocation();
			}
		});
	},
	
	disableForsquareLocationsList: function() {
		var list = $('#foursquareVenueId');
		if (list.find('option').length > 0) {
			list.removeAttr('disabled');
		} else {
			list.attr('disabled', true);
		}
		$('#foursquareVenueId').trigger("liszt:updated");
	},
	
	chooseForsquareLocation: function() {
		var list = $('#foursquareVenueId');
		var selectedItem = list.find(":selected");
		$('#category').val(selectedItem.data('category'));
		$('#locationName').val(selectedItem.data('name'));
		$('#foursquareVenueId').trigger('change');
	}
}

$(document).ready(function() {
	userNote.disableForsquareLocationsList();
	$('#foursquareVenueId').chosen().change(function(){
		userNote.chooseForsquareLocation();
	});
})
