var mediaContentEditor = {
	selectedFile: '',

	showFile: function(input) {
		if (input.files && input.files[0]) {
			if (input.files[0].type == 'image/jpeg' || input.files[0].type == 'video/quicktime' || input.files[0].type == 'video/mp4' || input.files[0].type == 'video/x-m4v') {
				var fileType = input.files[0].type;
				var reader = new FileReader();
				
				if ( window.webkitURL ) {
					var fileUrl = window.webkitURL.createObjectURL( input.files[0] );
				} else if ( window.URL && window.URL.createObjectURL ) {
					var fileUrl = window.URL.createObjectURL( input.files[0] );
				} else {
					return true;
				}

				reader.onload = function (e) {
					if (fileType == 'video/quicktime' || fileType == 'video/mp4' || input.files[0].type == 'video/x-m4v') {
						$('#avatarVideo').attr('src', fileUrl);
						$('#avatarVideo').show();
						$('#avatarImage').hide();
					} else {
						$('#avatarImage').css({
							"background-image" : "url("+ fileUrl +")",
							"background-color": '#000000',
						});
						$('#avatarVideo').removeClass('js-video-media-container');
						$('#avatarImage').show();
						$('#avatarVideo').hide();
					}
				}
				reader.readAsDataURL(input.files[0]);
				$('[name="removeAvatar"]').val(0);
			}
			else {
				parent.jAlert('You can upload jpg, jpeg, mov, mp4, m4v files only', 'Error');
				$('#avatarImage').css({
					"background-image" : mediaContentEditor.selectedFile
				});
				$(input).val(null)
			}
		}

	},

	removeFile: function(defaultImageUrl, fileType) {
		var container = $('.js-'+fileType+'-media-container');
		if (fileType == 'image') {
			container.css({
				"background-image" : "url("+ defaultImageUrl +")"
			});
		} else {
			container.attr('src', defaultImageUrl);
		}
		$('.note_media').hide();
		container.show();
		var fileInput = $('[name="media"]');
		fileInput.replaceWith(fileInput.val('').clone(true));
		return false;
	},

	beforeSubmitAvatar: function() {
		var input = document.getElementsByName("avatar")[0];
		if (input.files && input.files[0]) {
			if ( window.webkitURL ) {
				var fileUrl = window.webkitURL.createObjectURL( input.files[0] );
			} else if ( window.URL && window.URL.createObjectURL ) {
				var fileUrl = window.URL.createObjectURL( input.files[0] );
			} else {
				return true;
			}
			var isValidForm = true;
			$.ajax({
				url: fileUrl,
				async: false,
				error: function(data){
					parent.jAlert('Please select correct file', 'Error');
					isValidForm = false;
				}
			});
			return isValidForm;
		}
		else return true;
	},
}

$(document).ready(function() {
	mediaContentEditor.selectedFile = $('#avatarImage').css('background-image');
});