var map = {
	lat: null,
	lng: null,
	options : {
		zoom: 15, // This number can be set to define the initial zoom level of the map
		mapTypeId: google.maps.MapTypeId.ROADMAP // This value can be set to define the map type ROADMAP/SATELLITE/HYBRID/TERRAIN
	},
	mapObject: null, 
	mapContainerId: 'map', 
	mapMarker: null,
	
	initMap: function() {
		map.mapObject = new google.maps.Map(document.getElementById(map.mapContainerId), map.options);
	},
	
	init: function(lat, lng) {
		if (typeof(lat) !== 'undefined' && typeof(lng) !== 'undefined' && lat && lng) {
			map.lat = lat;
			map.lng = lng;
		} else {
			map.lat = 38.506748;
			map.lng = -100.392912;
			map.options.zoom = 4;
		}
		
		map.options.center =  new google.maps.LatLng(map.lat, map.lng);
		map.mapObject = new google.maps.Map(document.getElementById(map.mapContainerId), map.options);
		
		if (typeof(lat) !== 'undefined' && typeof(lng) !== 'undefined' && lat && lng) {
			map.mapMarker = new google.maps.Marker({
				position: new google.maps.LatLng(map.lat, map.lng),
				map: map.mapObject,
			});
		}
		
		map.mapObject.addListener('click', function(e) {
			map.setMarker(e.latLng.lat(), e.latLng.lng());
			$('#js-address-search').val('');
		});

	},
	
	setMarker: function(lat, lng, changeZoom) {
		if (map.mapMarker) {
			map.mapMarker.setMap();
		}
		map.lat = lat;
		map.lng = lng;
		map.mapMarker = new google.maps.Marker({
			position: new google.maps.LatLng(map.lat, map.lng),
			map: map.mapObject,
		});
		var newCenter =  new google.maps.LatLng(map.lat, map.lng);
		map.mapObject.setCenter(newCenter);
		if (changeZoom) {
			map.mapObject.setZoom(15);
		}
		userNote.fillForsquareLocation();
		$('#latitude').val(map.lat);
		$('#longitude').val(map.lng);
	}
}