<?php
ini_set('display_errors', 1);
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require __DIR__ . '/../vendor/autoload.php';
require __DIR__. '/../vendor/utils/src/Registry.php';

//var_dump(URL); die();

// Run the application!
\Zend\Mvc\Application::init(require 'config/application.config.php')->run();

