<?php

namespace Auth\Controller;

use Auth\Service\UserWrapperFactory;
use Zend\View\Model\ViewModel;
use Auth\Service\AuthServiceController;
use Application\Model\User\SocialnetworkTable;
use Application\Lib\User;
use \Application\Lib\Image;

/**
 * Login or out using a standart login form or social service
 */
class IndexController extends AuthServiceController
{

	public function ready() {
		parent::ready();
		$this->userTable = new User();
	}

	/**
	 * login using login & password
	 */
	public function loginAction() {
		if($this->user->getId()) {
			return $this->redirect()->toRoute('home');
		}
		$this->setBreadcrumbs([], false);
		
		$form = new \Auth\Form\LoginForm();

		if($this->request->isPost()){
			$data = $this->request->getPost();
			$form->setData($data);
			if ($form->isValid()) {
				$data = $form->getData();
				$user = $this->user->checkLogin($data['login'], $data['password']);

				if($user) {
					try {
						$this->user->login('admin');
						return $this->redirect()->toRoute('home');
					}
					catch (\Exception $e) {
						$this->error = $e->getMessage();
					}
				}
				else{
					$form->get('login')->setMessages(array(
						'notvalid' => _('Login credentials are incorrect'),
					));
				}
			}
		}

		$result = new ViewModel(array(
			'form' => $form,
			'error' => $this->error,
		));
		$result->setTemplate('auth/index/login');
		return $result;

	}

	/**
	 * Logout
	 */
	public function logoutAction() {
		$this->user->logout();
		$this->redirect()->toRoute('home');
	}

}
