<?php
namespace Application\Lib;

use \Zend\Http\Client;
use Zend\Http\Response;

class APIClient extends Client {
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct(API_URL, array(
			'adapter' => 'Zend\Http\Client\Adapter\Curl',
			'curloptions' => array(
				CURLOPT_SSL_VERIFYPEER => false,
			),
		));
	}

	/**
	* 	
	* @param string $apiMethod
	* @param array $parameters
	* @return array
	*/
	public function getResult($apiMethod, $parameters = array(), $method = 'GET', $file = null, $sendForseMultipartFormdata = false) {
		$this->setUri(API_URL.$apiMethod);
		$method = strtoupper($method);
		$this->setMethod($method);
		if ($method == 'GET') {
			$this->setParameterGet($parameters);
		}
		else {
			$this->setParameterPost($parameters);
		}
		if (!empty($file)) {
			$this->setFileUpload($file['name'], $file['field'], isset($file['content'])?$file['content']:null, $file['type']);
		}
		
		if ($sendForseMultipartFormdata) {
			$this->setEncType(self::ENC_FORMDATA);
		}
		
		$response = $this->send();
		if ($response->isSuccess()) {
			$result = json_decode($response->getContent());
			
			if (isset($result->error) && $result->error) {
				throw new \Exception($result->message, (isset($result->code)? $result->code : 0));
			}
			else {
				return $result;
			}
			return;
		}

		if ($response->getStatusCode() === Response::STATUS_CODE_400) {
			if (($data = json_decode($response->getContent())) && $data->message) {
				throw new \Exception($data->message);
			}
		}
		
		throw new \Exception("API request failed");
	}
	
}
