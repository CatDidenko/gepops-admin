<?php

namespace Admin;

use Zend\Mvc\ModuleRouteListener;
use Zend\Http\Header\ContentEncoding;

class Module
{
    public function onBootstrap($e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(\Zend\Mvc\MvcEvent::EVENT_DISPATCH_ERROR, function($e) {
			$serviceManager = $e->getApplication()->getServiceManager();
			$controller = $e->getApplication()->getMvcEvent()->getController();
			if (strpos($controller, 'Admin') === 0) {
				$layout = $serviceManager->get('viewManager')->getViewModel();
				$layout->setTemplate('admin/layout');
			}
		});
		
		if(defined('GZIP_OUTPUT') && GZIP_OUTPUT) {
			$eventManager->attach('finish', array($this, 'compressOutput'), -100000);
		}
    }

    public function init(\Zend\ModuleManager\ModuleManager $moduleManager)
    {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach(__NAMESPACE__, 'dispatch', function($e) {
            // This event will only be fired when an ActionController under the MyModule namespace is dispatched.
            $controller = $e->getTarget();
            $controller->layout('admin/layout');
        }, 100);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            ),
        );

    }
    
    public function compressOutput($e) {
		$response = $e->getResponse();
		if (get_class($response) !== 'Zend\Console\Response') {
			$content = $response->getBody();
			$content = str_replace('  ', ' ', str_replace("\r", ' ', str_replace("\t", ' ', $content)));

			if($e->getRequest()->getHeader('Accept-Encoding') && $e->getRequest()->getHeader('Accept-Encoding')->hasEncoding('gzip')) {
				$response->getHeaders()->addHeader(new ContentEncoding('gzip'));
				$content = gzencode($content, 9);
			}

			$response->setContent($content);
		}
	}
}
