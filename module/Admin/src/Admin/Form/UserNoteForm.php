<?php
namespace Admin\Form;

use Admin\Helper\FileHelper;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\Validator\File\ImageSize;
use Zend\Validator\File\MimeType;

class UserNoteForm extends \Application\Form\Form {
	/**
	 * @var Zend\InputFilter\InputFilter;
	 */
	protected $inputFilter;

	/**
	 * 
	 * @var string
	 */
	private $action;

	const MAX_FILE_SIZE = 104857600; // 100 Mb

	/**
	 * constructor
	 * 
	 * @param string $level
	 * @return UserEditForm
	 */
	public function __construct($action = 'edit', $userId = null) {
		parent::__construct('noteedit');

		$this->setAttribute('method', 'post');
		$this->setAttribute('class', 'formWrapp ajaxForm');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setAttribute('onsubmit', 'return mediaContentEditor.beforeSubmitAvatar();');
		$this->action = $action;

		$this->add(array(
			'name' => 'csrf',
			'type' => 'Zend\Form\Element\Csrf',
			'options' => array(
				'csrf_options' => array(
					'messages' => array(
						\Zend\Validator\Csrf::NOT_SAME => _('The form submitted did not originate from the expected site'),
					),
					'timeout' => null,
				),
			),
		));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden',
			),
		));
		
		$this->add(array(
			'name' => 'latitude',
			'attributes' => array(
				'type' => 'hidden',
			),
		));
		
		$this->add(array(
			'name' => 'longitude',
			'attributes' => array(
				'type' => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'address',
			'options' => array(
				'label' => _('Type address or choose location on the map'),
			),
			'attributes' => array(
				'class' => 'input-big',
				'id' => 'js-address-search'
			),
		));
		
		$this->add(array(
			'name' => 'foursquareVenueId',
			'type' => 'select',
			'options' => array(
				'label' => _('Foursquare location finder'),
			),
			'attributes' => array(
				'class' => 'input-big chosen-select',
				'data-placeholder' => _("Choose a location from Foursquare..."),
			),
		));
		
		$this->add(array(
			'name' => 'locationName',
			'attributes' => array(
				'type' => 'hidden',
			),
		));
		
		$this->add(array(
			'name' => 'category',
			'options' => array(
				'label' => _('Category'),
			),
			'attributes' => array(
				'class' => 'input-big',
				'readonly' => true,
			),
		));
		
		$this->add(array(
			'name' => 'distance',
			'options' => array(
				'label' => _('Note Radius in meters/Possible values between 50 mt-2 miles'),
			),
			'attributes' => array(
				'class' => 'input-big',
			),
		));

		$this->add(array(
			'name' => 'description',
			'type' => 'textarea',
			'options' => array(
				'label' => _('Description'),
			),
			'attributes' => array(
				'class' => 'input-big',
				'maxlength' => 140,
			)
		));
		
		$this->add(array(
			'name' => 'expiryDate',
			'type' => 'select',
			'options' => array(
				'label' => _('Live Note'),
				'options' => [
					'0' => _('Keep it Permanently'),
					'86400000' => _('24 Hours'),
					'172800000' => _('2 Days'),
					'604800000' => _('1 Week'),
					'2592000000' => _('1 Month'),
					'7776000000' => _('3 Months'),
				]
			),
			'attributes' => array(
				'class' => 'input-big chosen-select',
			)
		));

		$this->add(array(
			'name' => 'media',
			'type' => 'file',
			'attributes' => array(
				'onchange' => 'mediaContentEditor.showFile(this);',
				'accept' => ".jpg,.mov,.mp4,.m4v",
			),
		))

		->add(array(
			'name' => 'removeMedia',
			'type' => 'hidden',
			'attributes' => array(
				'value' => '0',
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => ($action == 'edit')? _('Save') : _('Create'),
			))
		);

		$this->add(array(
			'name' => 'cancel',
			'type' => '\Zend\Form\Element\Button',
			'options' => array(
				'label' => _('Cancel'),
			),
			'attributes' => array(
				'value' => _('Cancel'),
				'class' => 'clear-btn popup_cancel',
				'onclick' => "common.cancelChanges(".'"'.htmlspecialchars(addslashes(URL.'admin/user-note/index/'.$userId)).'")',
			))
		);
	}


	public function getInpFilter() {
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$factory = new InputFactory();

			$notemptyValidator = array(
				'name' => 'not_empty',
				'options' => array (
					'messages' => array(
						\Zend\Validator\NotEmpty::IS_EMPTY => _("This field is required"),
					),
				),
				'break_chain_on_failure' => true,
			);

			$inputFilter->add($factory->createInput(array(
				'name' => 'latitude',
				'required' => true,
				'validators' => array(
					$notemptyValidator,
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'longitude',
				'required' => true,
				'validators' => array(
					$notemptyValidator,
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'locationName',
				'required' => true,
				'validators' => array(
					$notemptyValidator,
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'foursquareVenueId',
				'required' => true,
				'validators' => array(
					$notemptyValidator,
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'category',
				'required' => true,
				'validators' => array(
					$notemptyValidator,
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'distance',
				'required' => true,
				'filters' => [
					['name' => 'Int'],
				],
				'validators' => array(
					$notemptyValidator,
					new \Zend\Validator\Between(array(
						'min' => 50,
						'max' => 3218,
						'message' => _('Note Radius should be between 50 meters and 2 miles'),
					)),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'description',
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					$notemptyValidator,
					new \Zend\Validator\StringLength(array(
						'min' => 0,
						'max' => 140,
						'message' => _('Description can not be longer than 140 characters'),
					)),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name' => 'media',
				'required' => ($this->action == 'add'),
				'validators' => array(
					array(
						'name' => '\Zend\Validator\File\Extension',
						'options' => array (
							'extension' => array('jpg', 'mov', 'mp4', 'm4v'),
							'messages' => array(
								\Zend\Validator\File\Extension::FALSE_EXTENSION => "You can upload jpg/mov files only",
							),
						),
						'break_chain_on_failure' => true,
					),
					new \Zend\Validator\File\Size(array(
						'max' => self::MAX_FILE_SIZE,
						'messages' => array(
							\Zend\Validator\File\Size::TOO_BIG => _('Your file should be less than 100 Mb'),
						)
					)),
					['name' => 'filemimetype', 'options' => [
						'mimeType' => implode(',', FileHelper::getAllowedMimeTypes()),
						'messages' => [
							MimeType::NOT_DETECTED => _('Invalid file type'),
							MimeType::FALSE_TYPE => _('Invalid file type'),
						],
					]],
					['name' => 'callback', 'options' => [
						'message' => _('Photo resolution should be 720 x 1280 px. Video resolution should has resolution of 720 px x 1280 px and duration up to 10 seconds.'),
						'callback' => function($value, $context) {
							$finfo = finfo_open(FILEINFO_MIME_TYPE);
							//var_dump(FileHelper::isImage(finfo_file($finfo, $value['tmp_name'])));exit;
							if (FileHelper::isImage(finfo_file($finfo, $value['tmp_name']))) {
								$imageValidator = new ImageSize([
									'minWidth' => 720,
									'maxWidth' => 720,
									'minHeight' => 1280,
									'maxHeight' => 1280,
								]);
								return $imageValidator->isValid($value);
							} else {
								$getID3 = new \getID3();
								$info = $getID3->analyze($value['tmp_name']);
								if ($info && isset($info['playtime_seconds']) && isset($info['video'])) {
									if (
										(10 >= (int)$info['playtime_seconds']) ||
										(720 == (int)$info['video']['resolution_x']) ||
										(1280 == (int)$info['video']['resolution_y'])
									) {
										return true;
									}
								}
								return false;
							}
						},
					]],
				),
			)));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
	
	public function setForsquareLocationOptions($list) {
		$options = [];
		if (!empty($list->response->venues)) {
			foreach ($list->response->venues as $venue) {
				$category = '';
				if (isset($venue->categories[0])) {
					$category = $venue->categories[0]->name;
				}
				$item = [
					'label' => $venue->name . '/' . $category,
					'value' => $venue->id,
					'attributes' => array(
						'data-category' => $category,
						'data-name' => $venue->name,
					),
				];
				
				$options[] = $item;
			}
		}
		
		$this->get('foursquareVenueId')->setValueOptions( $options);
	}
}
