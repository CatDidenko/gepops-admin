<?php
namespace Admin\Form;

use Application\Lib\APIClient;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class UserEditForm extends \Application\Form\Form {
	/**
	 * @var Zend\InputFilter\InputFilter;
	 */
	protected $inputFilter;

	/**
	 * 
	 * @var string
	 */
	private $action;

	/**
	 * 
	 * @var int
	 */
	private $userId;

	const MAX_FILE_SIZE = 10485760; // 10 Mb

	/**
	 * constructor
	 * 
	 * @param string $level
	 * @return UserEditForm
	 */
	public function __construct($action = 'edit', $userId = null) {
		parent::__construct('useredit');

		$this->setAttribute('method', 'post');
		$this->setAttribute('class', 'formWrapp ajaxForm userEditForm');
		$this->setAttribute('enctype', 'multipart/form-data');
		$this->setAttribute('onsubmit', 'return avatarEditor.beforeSubmitAvatar();');
		$this->action = $action;
		$this->userId = $userId;

		$this->add(array(
			'name' => 'csrf',
			'type' => 'Zend\Form\Element\Csrf',
			'options' => array(
				'csrf_options' => array(
					'messages' => array(
						\Zend\Validator\Csrf::NOT_SAME => _('The form submitted did not originate from the expected site'),
					),
					'timeout' => null,
				),
			),
		));

		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type' => 'hidden',
			),
		));

		$this->add(array(
			'name' => 'firstName',
			'options' => array(
				'label' => _('First Name'),
			),
			'attributes' => array(
				'class' => 'input-big',
				'maxlength' => 100,
			),
		));
		
		$this->add(array(
			'name' => 'lastName',
			'options' => array(
				'label' => _('Last Name'),
			),
			'attributes' => array(
				'class' => 'input-big',
				'maxlength' => 100,
			),
		));

		$this->add(array(
			'name' => 'description',
			'type' => 'textarea',
			'options' => array(
				'label' => _('Description'),
			),
			'attributes' => array(
				'class' => 'input-big',
				'maxlength' => 200,
			)
		));

		$this->add(array(
			'name' => 'avatar',
			'type' => 'file',
			'attributes' => array(
				'onchange' => 'avatarEditor.showFile(this);',
				'accept' => "image/*",
			),
		))

		->add(array(
			'name' => 'removeAvatar',
			'type' => 'hidden',
			'attributes' => array(
				'value' => '0',
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type' => 'submit',
				'value' => ($action == 'edit')? _('Save') : _('Create'),
			))
		);

		$this->add(array(
			'name' => 'cancel',
			'type' => '\Zend\Form\Element\Button',
			'options' => array(
				'label' => _('Cancel'),
			),
			'attributes' => array(
				'value' => _('Cancel'),
				'class' => 'clear-btn popup_cancel',
				'onclick' => "common.cancelChanges(".'"'.htmlspecialchars(addslashes(URL.'admin/user')).'")',
			))
		);
	}


	public function getInpFilter() {
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$factory = new InputFactory();

			$notemptyValidator = array(
				'name' => 'not_empty',
				'options' => array (
					'messages' => array(
						\Zend\Validator\NotEmpty::IS_EMPTY => _("This field is required"),
					),
				),
				'break_chain_on_failure' => true,
			);

			$inputFilter->add($factory->createInput(array(
				'name' => 'firstName',
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					$notemptyValidator,
					new \Zend\Validator\StringLength(array(
						'min' => 0,
						'max' => 100,
						'message' => _('First name can not be longer than 100 characters'),
					)),
					['name' => 'callback', 'options' => [
						'message' => _('First Name and Last Name are not unique.'),
						'callback' => function($value, $context) {
							$api = new APIClient();
							return $api->getResult('admin/userNameUnique', [
								'firstName' => $value,
								'lastName' => $context['lastName'],
								'excludingUserId' => $this->userId,
							])->result;
						},
					],],
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'lastName',
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					$notemptyValidator,
					new \Zend\Validator\StringLength(array(
						'min' => 0,
						'max' => 100,
						'message' => _('Last name can not be longer than 100 characters'),
					)),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name' => 'description',
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					$notemptyValidator,
					new \Zend\Validator\StringLength(array(
						'min' => 0,
						'max' => 200,
						'message' => _('Description can not be longer than 200 characters'),
					)),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name' => 'avatar',
				'required' => ($this->action == 'add'),
				'validators' => array(
					array(
						'name' => '\Zend\Validator\File\Extension',
						'options' => array (
							'extension' => array('png', 'jpg', 'jpeg'),
							'messages' => array(
								\Zend\Validator\File\Extension::FALSE_EXTENSION => "You can upload jpg, jpeg, png files only",
							),
						),
						'break_chain_on_failure' => true,
					),
					new \Zend\Validator\File\Size(array(
						'max' => self::MAX_FILE_SIZE,
						'messages' => array(
							\Zend\Validator\File\Size::TOO_BIG => _('Your image should be less than 10 Mb'),
						)
					)),
				),
			)));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}
