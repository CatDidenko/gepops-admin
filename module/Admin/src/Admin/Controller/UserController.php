<?php
namespace Admin\Controller;

use Application\Lib\AppController;
use Application\Lib\User;
use Zend\View\Model\ViewModel;
use \Application\Lib\APIClient;

class UserController extends AppController {
	/**
	 * @var \Zend\Form\Form
	 */
	var $form;

	/**
	 * 
	 * @var APIClient
	 */
	var $api;


	public function ready() {
		parent::ready();
		
		$this->api = new APIClient();
	}

	/**
	 * return form
	 * 
	 * @return \Zend\Form\Form
	 */
	public function getForm($action = 'edit', $id = null) {
		if(is_null($this->form)) {
			$this->form = new \Admin\Form\UserEditForm($action, $id);
		}
		return $this->form;
	}

	
	public function indexAction() {
		$this->layout()->bodyClass = 'user';

		$result = array(
			'canAdd' => $this->user->isAllowed('Admin\Controller\User', 'add'),
			'canDelete' => false, //$this->user->isAllowed('Admin\Controller\User', 'save'),
		);
		$this->renderHtmlIntoLayout('submenu', 'admin/user/submenu.phtml', $result);
		return $result;
	}

	public function editAction() {
		$id = (int)$this->params('id',0);

		if(!$id) {
			return $this->redirect()->toRoute('admin', array('controller' => 'user', 'action' => 'add'));
		}
		
		$this->setBreadcrumbs(['admin/user' => 'Manage Businesses',]);
		$form = $this->getForm('edit', $id);
		$canEdit = $this->user->isAllowed('Admin\Controller\User', 'save');
		try {
			$user = $this->api->getResult('admin/getUser', ['id' => $id]);
			$form->setData((array)$user);
		}
		catch(\Exception $e) {
			return $this->notFoundAction();
		}

		if ($this->request->isPost()) {
			if ($canEdit){
				$data = array_merge_recursive($this->request->getPost()->toArray(), $this->request->getFiles()->toArray());
				$form->setData($data);
				if ($form->isValid()) {
					$data = $form->getData();
					$file = $data['avatar'];
					unset($data['avatar']);
					$fileData = null;
					if (!empty($file['name'])) {
						$fileData = array(
							'name' => $file['name'],
							'field' => 'avatar',
							'content' => file_get_contents($file['tmp_name']),
							'type' => $file['type'],
						);
					}
					
					try {
						$this->api->getResult('admin/updateUser', $data, "POST", $fileData, true);
						$this->redirect()->toUrl(URL.'admin/user/');
					} catch (\Exception $e) {
						$this->error = $e->getMessage();
					}
				}
			}
			else {
				$this->error = _('You do not have enough permissions to make changes');
			}
		}
		$form->get('avatar')->setValue(null);

		return  new ViewModel(array(
			'form' => $form,
			'error' => $this->error,
			'title' => _('Edit Business'),
			'item' => $user,
		));
	}

	public function addAction(){
		$this->setBreadcrumbs(['admin/user' => 'Manage Businesses',]);
		$form = $this->getForm('add');

		if ($this->request->isPost()) {
			$data = array_merge_recursive($this->request->getPost()->toArray(), $this->request->getFiles()->toArray());
			$form->setData($data);
			if ($form->isValid()) {
				$data = $form->getData();
				$file = $data['avatar'];
				unset($data['avatar']);
				
				try {
					$this->api->getResult('admin/addUser', $data, "POST", array(
						'name' => $file['name'],
						'field' => 'avatar',
						'content' => file_get_contents($file['tmp_name']),
						'type' => $file['type'],
					));
					$this->redirect()->toUrl(URL.'admin/user/');
				} catch (\Exception $e) {
					$this->error = $e->getMessage();
				}
			}
		}
		$form->get('avatar')->setValue(null);
		
		$result =  new ViewModel(array(
			'form' => $form,
			'error' => $this->error,
			'title' => _('Create New Business'),
		));
		$result->setTemplate('admin/user/edit.phtml');
		return $result;
	}


	public function deleteAction() {
		$id = (int)$this->request->getPost('id', 0);
		$this->userTable->delete($id);
		return $this->getResponse()->setContent('OK');
	}


	public function listAction() {
		$count = (int)$this->params()->fromQuery('count', 25);
		$pos = (int)$this->params()->fromQuery('posStart', 0);
		$params = $this->resolveParams();
		$orderby = $this->resolveOrderby();

		$params = array_merge($params, $orderby, [
			'limit' => $count,
			'offset' => $pos,
		]);
		
		$data = $this->api->getResult('admin/getUsers', $params);

		$xmlResult = new ViewModel(array(
			'pos' => $pos,
			'total' => $data->total,
			'list' => $data->list,
			'isAllowedDelete' => $this->user->isAllowed('Admin\Controller\User', 'delete'),
		));
		$xmlResult->setTerminal(true);
		return $xmlResult;
	}

	/**
	 * apply filters
	 * 
	 * @return array
	 */
	protected function resolveParams() {
		$params = array();

		$flPid = $this->params()->fromQuery('flPid');
		if(trim($flPid) !== '') {
			$params ['id'] = (int)$flPid;
		}

		$flFirstName = $this->params()->fromQuery('flFirstName');
		if(trim($flFirstName) !== '') {
			$params ['firstName']= $flFirstName;
		}
		
		$flLastName = $this->params()->fromQuery('flLastName');
		if(trim($flLastName) !== '') {
			$params ['lastName']= $flLastName;
		}
		
		return $params;
	}

	/**
	 * return orderby rule for list ordering
	 * 
	 * @return string
	 */
	protected function resolveOrderby() {
		$orderby='id';
		$orderdir='desc';

		if(isset($_GET['orderby'])) {
			switch($_GET['order']) {
				case 'asc': $orderdir='asc'; break;
				default: $orderdir='desc';
			}
			switch($_GET['orderby']) {
				case 1: $orderby="id"; break;
				case 3: $orderby="firstName"; break;
				case 4: $orderby="lastName"; break;
				default: $orderby="id"; break;
			}
		}

		return [
			'orderby' => $orderby,
			'order' => $orderdir,
		];
	}

}
