<?php
namespace Admin\Controller;

use Application\Lib\AppController;
use Application\Lib\User;
use Zend\View\Model\ViewModel;
use \Application\Lib\APIClient;

class UserNoteController extends AppController {
	/**
	 * @var \Zend\Form\Form
	 */
	var $form;

	/**
	 * 
	 * @var APIClient
	 */
	var $api;


	public function ready() {
		parent::ready();
		
		$this->api = new APIClient();
	}

	/**
	 * return form
	 * 
	 * @return \Zend\Form\Form
	 */
	public function getForm($action = 'edit', $id = null) {
		if(is_null($this->form)) {
			$this->form = new \Admin\Form\UserNoteForm($action, $id);
		}
		return $this->form;
	}
	
	protected function getUser() {
		$id = $this->params()->fromRoute('id', 0);

		if(!$id) {
			throw new \Exception();
		}
		$user = $this->api->getResult('admin/getUser', ['id' => $id]);
		
		return $user;
	}
	
	public function indexAction() {
		$this->setBreadcrumbs(['admin/user' => 'Manage Businesses',]);
		$this->layout()->bodyClass = 'user';
		
		try {
			$user = $this->getUser();
		}
		catch(\Exception $e) {
			return $this->redirect()->toRoute('admin', array('controller' => 'user'));
		}

		$result = array(
			'user' => $user,
			'canAdd' => $this->user->isAllowed('Admin\Controller\User', 'add'),
			'canDelete' => false, //$this->user->isAllowed('Admin\Controller\User', 'save'),
		);
		$this->renderHtmlIntoLayout('submenu', 'admin/user-note/submenu.phtml', $result);
		return $result;
	}

	public function editAction() {
		$id = $this->params('id',0);
		if(!$id) {
			return $this->redirect()->toRoute('admin', array('controller' => 'user'));
		}
		
		try {
			$note = $this->api->getResult('admin/getNote', ['id' => $id]);
			$user = $this->api->getResult('admin/getUser', ['id' => $note->createdBy]);
		}
		catch(\Exception $e) {
			return $this->redirect()->toRoute('admin', array('controller' => 'user'));
		}

		$form = $this->getForm('edit', $user->id);
		$form->setData($this->processNoteDataForForm($note));
		$form->setForsquareLocationOptions($this->getForsquareLocationList($note->location->latitude, $note->location->longitude));

		$this->setBreadcrumbs([
			'admin/user' => 'Manage Businesses',
			'admin/user-note/index/'.$user->id => (_('List of notes by '). $user->firstName . ' ' . $user->lastName)
		]);
		
		$canEdit = $this->user->isAllowed('Admin\Controller\User', 'save');

		if ($this->request->isPost()) {
			if ($canEdit){
				$data = array_merge_recursive($this->request->getPost()->toArray(), $this->request->getFiles()->toArray());
				if (!empty($data['latitude']) && !empty($data['longitude'])) {
					$form->setForsquareLocationOptions($this->getForsquareLocationList($data['latitude'], $data['longitude']));
				}
				$form->setData($data);
				
				if ($form->isValid()) {
					$data = $form->getData();
					$file = $data['media'];
					unset($data['media']);
					$fileData = null;
					if (!empty($file['name'])) {
						$fileData = array(
							'name' => $file['name'],
							'field' => 'media',
							'content' => file_get_contents($file['tmp_name']),
							'type' => $file['type'],
						);
					}
					try {
						$result = $this->api->getResult('admin/updateNote', $data, "POST", $fileData, true);
						$this->redirect()->toUrl(URL.'admin/user-note/index/'.$user->id);
					} catch (\Exception $e) {
						$this->error = $e->getMessage();
					}
				}
			}
			else {
				$this->error = _('You do not have enough permissions to make changes');
			}
		}
		$form->get('media')->setValue(null);

		return  new ViewModel(array(
			'form' => $form,
			'error' => $this->error,
			'title' => _('Edit Note by ') .  $user->firstName . ' ' . $user->lastName,
			'item' => $note,
			
		));
	}

	protected function processNoteDataForForm($note) {
		$noteData = (array)$note;//var_dump($noteData);
		$noteData = array_merge($noteData, (array)$noteData['location']);
		if (!empty($noteData['expiryDate'])) {
			$noteData['expiryDate'] = $noteData['expiryDate'] - $noteData['creationDate'];
		}
		$noteData['locationName'] = $noteData['name'];
		//var_dump($noteData);die();
		return $noteData;
	}
	
	public function addAction(){
		$this->layout()->bodyClass = 'edit-page';
		
		try {
			$user = $this->getUser();
		}
		catch(\Exception $e) {
			return $this->redirect()->toRoute('admin', array('controller' => 'user'));
		}
		
		$form = $this->getForm('add', $user->id);
		
		$this->setBreadcrumbs([
			'admin/user' => 'Manage Businesses',
			'admin/user-note/index/'.$user->id => (_('List of notes by '). $user->firstName . ' ' . $user->lastName),
		]);

		if ($this->request->isPost()) {
			$data = array_merge_recursive($this->request->getPost()->toArray(), $this->request->getFiles()->toArray());
			if (!empty($data['latitude']) && !empty($data['longitude'])) {
				$form->setForsquareLocationOptions($this->getForsquareLocationList($data['latitude'], $data['longitude']));
			}
			$form->setData($data);
			
			if ($form->isValid()) {
				$data = $form->getData();
				$file = $data['media'];
				unset($data['media']);
				$data['userId'] = $user->id;
				try {
					$result = $this->api->getResult('admin/addNote', $data, "POST", array(
						'name' => $file['name'],
						'field' => 'media',
						'content' => file_get_contents($file['tmp_name']),
						'type' => $file['type'],
					));
					$this->redirect()->toUrl(URL.'admin/user-note/index/'.$user->id);
				} catch (\Exception $e) {
					$this->error = $e->getMessage();
				}
			}
		}
		$form->get('media')->setValue(null);
		
		$result =  new ViewModel(array(
			'form' => $form,
			'error' => $this->error,
			'title' => _('Create New Note by ') . $user->firstName . ' ' . $user->lastName,
		));
		$result->setTemplate('admin/user-note/edit.phtml');
		return $result;
	}


	public function deleteAction() {
		$id = (int)$this->request->getPost('id', 0);
		$this->userTable->delete($id);
		return $this->getResponse()->setContent('OK');
	}


	public function listAction() {
		$id = $this->params()->fromQuery('userId', 0);
		$count = (int)$this->params()->fromQuery('count', 25);
		$pos = (int)$this->params()->fromQuery('posStart', 0);
		$params = $this->resolveParams();
		$orderby = $this->resolveOrderby();

		$params = array_merge($params, $orderby, [
			'limit' => $count,
			'offset' => $pos,
			'userId' => $id,
		]);

		$data = $this->api->getResult('admin/getNotes', $params);

		$xmlResult = new ViewModel(array(
			'pos' => $pos,
			'total' => $data->total,
			'list' => $data->list,
			'isAllowedDelete' => $this->user->isAllowed('Admin\Controller\User', 'delete'),
			'userId' => $id,
		));
		$xmlResult->setTerminal(true);
		return $xmlResult;
	}

	/**
	 * apply filters
	 * 
	 * @return array
	 */
	protected function resolveParams() {
		$params = array();

		$flPid = $this->params()->fromQuery('flPid');
		if(trim($flPid) !== '') {
			$params ['id'] = (int)$flPid;
		}

		$flContentType = $this->params()->fromQuery('flContentType');
		if(trim($flContentType) !== '') {
			$params ['type']= $flContentType;
		}
		
		$flLocation = $this->params()->fromQuery('flLocation');
		if(trim($flLocation) !== '') {
			$params ['locationName']= $flLocation;
		}
		
		return $params;
	}

	/**
	 * return orderby rule for list ordering
	 * 
	 * @return string
	 */
	protected function resolveOrderby() {
		$orderby='id';
		$orderdir='desc';

		if(isset($_GET['orderby'])) {
			switch($_GET['order']) {
				case 'asc': $orderdir='asc'; break;
				default: $orderdir='desc';
			}
			switch($_GET['orderby']) {
				case 1: $orderby="id"; break;
				case 4: $orderby="locationName"; break;
				case 5: $orderby="creationDate"; break;
				case 6: $orderby="expiryDate"; break;
				default: $orderby="id"; break;
			}
		}

		return [
			'orderby' => $orderby,
			'order' => $orderdir,
		];
	}
	
	protected function getForsquareLocationList($lat, $lng) {
		$items = file_get_contents(sprintf('https://api.foursquare.com/v2/venues/search?ll=%s,%s&oauth_token='.FORSQUARE_TOKEN . '&v='.date('Ymd'), $lat, $lng));
		$list = json_decode($items);
		return $list;
	}
	
	public function getForsquareLocationAction() {
		$lat = $this->params()->fromQuery('lat', 0);
		$lng = $this->params()->fromQuery('lng', 0);
		
		$list = $this->getForsquareLocationList($lat, $lng);
		
		$response = [];
		if (!empty($list->response->venues)) {
			foreach ($list->response->venues as $venue) {
				$item = [
					'id' => $venue->id,
					'name' => $venue->name,
				];
				if (isset($venue->categories[0])) {
					$item['category'] = $venue->categories[0]->name;
				}
				$response[] = $item;
			}
		}
		return $this->sendJSONResponse([
			'list' => $response,
		]);
	}

}
