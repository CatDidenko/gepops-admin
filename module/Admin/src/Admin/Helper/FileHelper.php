<?php

namespace Admin\Helper;

class FileHelper
{

	protected static $allowedMimeTypes = [
		'image/jpeg',
		'video/quicktime',
		'video/mp4',
	];

	public static function getAllowedMimeTypes() {
		return self::$allowedMimeTypes;
	}

	public static function isImage($mimeType) {
		return $mimeType == 'image/jpeg';
	}
}
